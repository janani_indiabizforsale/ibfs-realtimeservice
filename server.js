var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 51515;
var redis = require('redis');
var redis_port = 6379; //default port
var redis_host = '127.0.0.1';

var client = redis.createClient(redis_port, redis_host);

//Serve html
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});


//Redis connection
client.on('connect', function() {
    console.log('Redis client connected');
});
client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});


//test redis 
client.set('my test key', 'my test value', redis.print);
client.get('my test key', function (error, result) {
    if (error) {
        console.log(error);
        throw error;
    }
    console.log('GET result ->' + result);
});

//Socket connection and events
//online_users - bitset in redis
io.on('connection', function(socket){
io.emit('chat message', 'new user connected');

var userId;
  socket.on('connected', function(id) {
  	userId = id = parseInt(id);
    io.emit('chat message', id + ' connected');
    // add id to redis
    client.setbit('online_users', userId, 1);
  });

  socket.on('disconnect', function() {
    io.emit('chat message', userId + ' disconnected');
    // remove from redis
    client.setbit('online_users', userId, 0);
  });

});


app.get('/user/:id', function (req, res) {
var status;
  client.multi([
    ['GETBIT', 'online_users', req.params.id]
  ]).exec((err, result) => {
    if(result){
      status = result[0];
      res.json(status);
    }
  });
})

//node listen
http.listen(port, function(){
  console.log('listening on *:' + port);
});
